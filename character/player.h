#include <utility>
#include <sstream>

//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_PLAYER_H
#define MINI_WUMPUS_PLAYER_H


#include "base_unit.h"
#include <sstream>
#include <iterator>

namespace miniwumpus {
    class Player : public BaseUnit {
    public:
        explicit Player(int character_id, std::weak_ptr<EnvMap> env_map);

        bool perform_action() override;

    private:
        typedef bool(Player::* member_function_ptr_type)(const std::vector<std::string> &user_input_args);

        typedef std::map<std::string, member_function_ptr_type> map_of_action_type;
        map_of_action_type map_of_actions;

        static std::vector<std::string> getUserInput();
    };
}

#endif //MINI_WUMPUS_PLAYER_H
