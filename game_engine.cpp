//
// Created by Erik on 7/5/2019.
//

#include "game_engine.h"
#include "character/player.h"
#include "character/wumpus.h"

miniwumpus::GameEngine::GameEngine(int map_width) {
    game_map.reset(new EnvMap(map_width));
    AddCharacterToMap();
}


void miniwumpus::GameEngine::AddCharacterToMap() {
    std::shared_ptr<Player> player(new Player(0, std::weak_ptr<EnvMap>(game_map)));
    std::shared_ptr<Wumpus> wumpus(new Wumpus(1, std::weak_ptr<EnvMap>(game_map)));
    characters.push_back(std::weak_ptr<BaseUnit>(player));
    characters.push_back(std::weak_ptr<BaseUnit>(wumpus));

    std::vector<std::shared_ptr<BaseUnit>> init_units{player, wumpus};

    game_map->AddInitCharacter(init_units);
}


miniwumpus::GameEngine::~GameEngine() = default;

void miniwumpus::GameEngine::GameStart() {
    bool finish_game = false;
    InitText();
    while (!finish_game) {
        for (auto &character: characters) {
            character.lock()->perform_action();

            finish_game = CheckGameState();

            if (finish_game) {
                break;
            }
        }
    }
    std::cout << " ----- \n";
    for (auto &character: characters) {
        std::cout << "Character: " << character.lock()->character_id << " has " << character.lock()->health
                  << " health left \n";
    }

    std::cout << "End game" << std::endl;
}

void miniwumpus::GameEngine::InitText() {
    std::cout << "Some introduction" << std::endl;
}

bool miniwumpus::GameEngine::CheckGameState() {
    for (auto &character: characters) {
        if (character.lock()->health <= 0) {
            return true;
        }
    }
    return false;
}
