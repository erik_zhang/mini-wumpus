//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_ENV_MAP_H
#define MINI_WUMPUS_ENV_MAP_H


#include <memory>
#include <map>
#include <iostream>
#include <vector>
#include "base_tile.h"
#include "../character/base_unit.h"
#include "../enums/directions.h"

namespace miniwumpus {
    class BaseTile;

    class BaseUnit;

    class EnvMap {
    private:
        //Tile id -> Tile
        std::map<int, std::shared_ptr<BaseTile>> tiles;
        //Character id -> Character
        std::map<int, std::shared_ptr<BaseUnit>> characters;
        int map_width;

    public:
        EnvMap() = delete;

        explicit EnvMap(int map_width);

        ~EnvMap();

        EnvMap(const EnvMap &) = delete;

        void AddInitCharacter(const std::vector<std::shared_ptr<BaseUnit>> &);

        bool MoveCharacter(int character_id, TravelDirection direction);

        bool AttackCharacter(int attacker_id, int defender_id);

        void OutputAvailablePlayerActionText(int character_id);

    private:
        std::map<TravelDirection, int> getInitNeighbours(int tile_id);

        void InitTiles();
    };


}

#endif //MINI_WUMPUS_ENV_MAP_H
