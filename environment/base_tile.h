//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_BASE_TILE_H
#define MINI_WUMPUS_BASE_TILE_H


#include <map>
#include <memory>
#include "env_map.h"
#include "../enums/directions.h"

namespace miniwumpus {
    class BaseUnit;

    class EnvMap;

    class BaseTile {
    private:
    protected:
        explicit BaseTile(int tile_id) : tile_id(tile_id) {}

    public:

        std::vector<std::weak_ptr<BaseUnit>> characters_at_tile;
        std::map<TravelDirection, std::weak_ptr<BaseTile>> neighbour_tiles;
        int tile_id;

        virtual void AddNeighbour(TravelDirection travelDirection, const std::weak_ptr<BaseTile> &tile_ptr);

        virtual void AddCharacter(const std::weak_ptr<BaseUnit> &unit_ptr);

        virtual ~BaseTile() = default;


    };

}
#endif //MINI_WUMPUS_BASE_TILE_H
