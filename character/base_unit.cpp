//
// Created by Erik on 7/5/2019.
//

#include "base_unit.h"

bool miniwumpus::BaseUnit::Travel(const std::vector<std::string> &direction) {
    try {
        return env_map.lock()->MoveCharacter(character_id, StringToTravelDir(direction[1]));
    } catch (...) {
        std::cout << "Could not determine direction: " << direction[1] << std::endl;
        return false;
    }
}

bool miniwumpus::BaseUnit::Attack(const std::vector<std::string> &user_input_arg) {
    try {
        int target_id = std::stoi(user_input_arg[1]);
        return env_map.lock()->AttackCharacter(character_id, target_id);
    } catch (...) {
        return false;
    }
}

bool miniwumpus::BaseUnit::take_damage(int dmg) {
    health -= dmg;
    return true;
}

miniwumpus::BaseUnit::BaseUnit(int character_id, std::weak_ptr<miniwumpus::EnvMap> env_map) : character_id(
        character_id), env_map(std::move(env_map)) {
}

