# mini-wumpus

Install
---
Use Cmake, or Make to compile the code

Linux:
- make

Windows: (Powershell) Using chocolatey package manager
Alt 1
- choco install make
- Stand in the directory
- To compile: make
- To clean up: make cleanwin

Alt 2
- Choco install mingw
- Mingw Toolchain location C:\ProgramData\chocolatey\lib\mingw\tools\install\mingw64



Description:
---
Mini-Wumpus is a small bare-bone game with very very limited functionality, with goal to display some C++(11) Code.
This utilizes only the std library and can be compiled with either Make or Cmake.

Since this isn't a tool I supposed it 'solves' the boredom problem... :)

GameEngine is supposed to just initiate items and trigger different events such as action for each character
(currently only player can perform action)

EnvMap is the owner of all tiles and character, as such any action which modifies gamestate should be done via it



How to play:
---
You can perform two actions, either "Travel" or "Attack":
1. Travel %direction
    - Travel West
2. Attack %id
    - Attack 1
    
    
3. Cheat sheet
    - Travel South
    - Attack 1