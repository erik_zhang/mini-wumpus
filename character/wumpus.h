//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_WUMPUS_H
#define MINI_WUMPUS_WUMPUS_H


#include "base_unit.h"

namespace miniwumpus {
    class Wumpus : public BaseUnit {
    public:
        explicit Wumpus(int character_id, std::weak_ptr<EnvMap> ptr);

        bool take_damage(int dmg) override;

        bool perform_action() override;
    };
}

#endif //MINI_WUMPUS_WUMPUS_H
