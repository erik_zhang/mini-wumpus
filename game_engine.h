//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_GAME_ENGINE_H
#define MINI_WUMPUS_GAME_ENGINE_H

#include <memory>
#include <vector>
#include "environment/env_map.h"
#include "character/base_unit.h"

namespace miniwumpus {

    class GameEngine {
    private:
        std::shared_ptr<EnvMap> game_map;
        std::vector<std::weak_ptr<BaseUnit>> characters;
    public:
        GameEngine() = delete;

        explicit GameEngine(int map_width);

        ~GameEngine();

        GameEngine(const GameEngine &) = delete;

        void GameStart();

    private:
        static void InitText();

        void AddCharacterToMap();

        bool CheckGameState();
    };


}
#endif //MINI_WUMPUS_GAME_ENGINE_H
