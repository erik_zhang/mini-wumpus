//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_BASE_UNIT_H
#define MINI_WUMPUS_BASE_UNIT_H

#include <iostream>
#include <vector>
#include <memory>
#include "../environment/env_map.h"

namespace miniwumpus {
    class BaseTile;

    class EnvMap;

    class BaseUnit {
    public:
        int character_id;

    protected:
        BaseUnit(int character_id, std::weak_ptr<EnvMap> env_map);

        std::weak_ptr<EnvMap> env_map;


    public:
        std::weak_ptr<BaseTile> location;
        int health = 100;

        virtual bool take_damage(int dmg);

        virtual bool perform_action() = 0;

        virtual ~BaseUnit() = default;

        bool Travel(const std::vector<std::string> &user_input_arg);

        bool Attack(const std::vector<std::string> &user_input_arg);

    };
}

#endif //MINI_WUMPUS_BASE_UNIT_H
