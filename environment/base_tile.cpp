//
// Created by Erik on 7/5/2019.
//

#include "base_tile.h"


void miniwumpus::BaseTile::AddNeighbour(miniwumpus::TravelDirection travelDirection,
                                        const std::weak_ptr<BaseTile> &tile_ptr) {
    neighbour_tiles.insert(std::make_pair(travelDirection, tile_ptr));
}

void miniwumpus::BaseTile::AddCharacter(const std::weak_ptr<BaseUnit> &unit_ptr) {
    characters_at_tile.push_back(unit_ptr);
}