//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_DIRECTIONS_H
#define MINI_WUMPUS_DIRECTIONS_H

namespace miniwumpus {
    enum class TravelDirection {
        North, East, South, West
    };

    inline const std::string TravelDirToString(TravelDirection dir) {
        switch (dir) {
            case TravelDirection::North:
                return "North";
            case TravelDirection::South:
                return "South";
            case TravelDirection::East:
                return "East";
            case TravelDirection::West:
                return "West";
            default:
                throw std::exception();
        }
    }

    inline const TravelDirection StringToTravelDir(std::string dir) {
        if (dir == "North") {
            return TravelDirection::North;
        } else if (dir == "South") {
            return TravelDirection::South;
        } else if (dir == "East") {
            return TravelDirection::East;
        } else if (dir == "West") {
            return TravelDirection::West;
        } else {
            throw std::exception();
        }
    }
}


#endif //MINI_WUMPUS_DIRECTIONS_H
