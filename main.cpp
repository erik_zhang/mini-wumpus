#include <iostream>
#include "game_engine.h"
#include "character/player.h"
#include "environment/sand_tile.h"

using namespace miniwumpus;

int main() {

    GameEngine *gameEngine = new GameEngine(4);
    gameEngine->GameStart();

    delete gameEngine;
    return 0;
}