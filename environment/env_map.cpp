//
// Created by Erik on 7/5/2019.
//
#include <algorithm>
#include <vector>
#include <iterator>
#include "env_map.h"
#include "sand_tile.h"
#include "../character/player.h"
#include "../character/wumpus.h"

miniwumpus::EnvMap::EnvMap(int map_width) {
    //Generate and create map
    this->map_width = map_width;
    InitTiles();
}

void miniwumpus::EnvMap::InitTiles() {
    for (int tile_id = 0; tile_id < map_width * map_width; tile_id++) {
        std::shared_ptr<BaseTile> new_tile(new SandTile(tile_id));

        tiles.insert(std::make_pair(tile_id, new_tile));
    }

    for (const auto &process_tile: tiles) {
        int tile_id = process_tile.first;
        auto neighbour_id_map = getInitNeighbours(tile_id);

        for (const auto &neighbour_id: neighbour_id_map) {
            int neighbour_tile_id = neighbour_id.second;
            std::weak_ptr<BaseTile> neighbour_tile_ptr = tiles[neighbour_tile_id];
            process_tile.second->AddNeighbour(neighbour_id.first, neighbour_tile_ptr);
        }
    }
}


std::map<miniwumpus::TravelDirection, int> miniwumpus::EnvMap::getInitNeighbours(int tile_id) {
    std::map<miniwumpus::TravelDirection, int> neighbours;
    // Has East neighbour
    if (tile_id < map_width * map_width - map_width) {
        neighbours.insert(std::make_pair(TravelDirection::East, tile_id + map_width));
    }
    // Has north neighbour
    if (tile_id % map_width != 0) {
        neighbours.insert(std::make_pair(TravelDirection::North, tile_id - 1));
    }
    // Has south neighbour
    if ((tile_id + 1) % map_width != 0) {
        neighbours.insert(std::make_pair(TravelDirection::South, tile_id + 1));
    }
    // Has west neighbour
    if (tile_id >= (map_width)) {
        neighbours.insert(std::make_pair(TravelDirection::West, tile_id - map_width));
    }

    return neighbours;
}

miniwumpus::EnvMap::~EnvMap() = default;

void miniwumpus::EnvMap::AddInitCharacter(const std::vector<std::shared_ptr<BaseUnit>> &init_characters) {
    int init_pos = 0;

    for (const auto &character_ptr : init_characters) {
        std::shared_ptr<BaseTile> tile = tiles[init_pos];
        characters.insert(std::make_pair(character_ptr->character_id, character_ptr));
        // Add location to character
        character_ptr->location = std::weak_ptr<BaseTile>(tile);
        // Add Character to location
        tile->AddCharacter(std::weak_ptr<BaseUnit>(character_ptr));
        init_pos++;
    }
}

bool miniwumpus::EnvMap::MoveCharacter(int character_id, miniwumpus::TravelDirection direction) {
    std::weak_ptr<BaseUnit> character = characters[character_id];
    std::weak_ptr<BaseTile> character_tile = character.lock()->location;
    int character_tile_id = character_tile.lock()->tile_id;
    auto neighbour_tile = character_tile.lock()->neighbour_tiles.find(direction);
    if (neighbour_tile != character_tile.lock()->neighbour_tiles.end()) {
        //Add character to the destination
        neighbour_tile->second.lock()->AddCharacter(character);
        //Remove character from source
        auto &character_vec = tiles[character_tile_id]->characters_at_tile;
        const auto target_ptr = std::find_if(character_vec.begin(), character_vec.end(),
                                             [character_id](const std::weak_ptr<BaseUnit> x) {
                                                 return (x.lock()->character_id == character_id);
                                             }
        );
        character_vec.erase(target_ptr);
        // Update character tile ptr
        character.lock()->location = neighbour_tile->second;
        return true;
    }
    // Failed to move character
    return false;
}

bool miniwumpus::EnvMap::AttackCharacter(int attacker_id, int defender_id) {
    std::weak_ptr<BaseUnit> character = characters[attacker_id];
    std::weak_ptr<BaseTile> character_tile = character.lock()->location;
    for (auto &other_char: character_tile.lock()->characters_at_tile) {
        if (other_char.lock()->character_id == defender_id) {
            std::weak_ptr<SandTile> stshp = std::dynamic_pointer_cast<SandTile>(character_tile.lock());
            if (stshp.lock() != nullptr) {
                std::cout << "Fight in a sand tile" << std::endl;
            }

            other_char.lock()->take_damage(150);
            std::cout << "Character id: " << attacker_id << " hits " << defender_id << "for 150" << std::endl;
            return true;
        }
    }
    std::cout << "Could not find character " << defender_id << " in the same room " << std::endl;
    return false;
}

void miniwumpus::EnvMap::OutputAvailablePlayerActionText(int character_id) {
    std::weak_ptr<BaseUnit> character = characters[character_id];
    std::weak_ptr<BaseTile> character_tile = character.lock()->location;

    std::cout << "You are at room id: " << character_tile.lock()->tile_id << std::endl;
    std::cout << "Available Action:\n";
    std::cout << "Travel in direction: \n";

    for (auto &direction: character_tile.lock()->neighbour_tiles) {
        std::cout << TravelDirToString(direction.first) << " ";
    }
    std::cout << std::endl;
    std::cout << "In room there is character with id (Attack)" << std::endl;
    for (auto &other_char: character_tile.lock()->characters_at_tile) {
        int other_id = other_char.lock()->character_id;
        std::cout << other_id << " ";
        if (other_id == character_id) {
            std::cout << "(you) ";
        }
        std::cout << std::endl;
    }
    std::cout << "\n";
}
