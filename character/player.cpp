//
// Created by Erik on 7/5/2019.
//

#include "player.h"

miniwumpus::Player::Player(int character_id, std::weak_ptr<EnvMap> env_map) : BaseUnit(character_id,
                                                                                       std::move(env_map)) {
    map_of_actions.insert(std::make_pair("Travel", &BaseUnit::Travel));
    map_of_actions.insert(std::make_pair("Attack", &BaseUnit::Attack));
}

bool miniwumpus::Player::perform_action() {
    bool event_success;
    std::vector<std::string> user_word_input;
    env_map.lock()->OutputAvailablePlayerActionText(character_id);
    while (true) {
        user_word_input = getUserInput();
        auto action_fun_it = map_of_actions.find(user_word_input[0]);
        if (action_fun_it == map_of_actions.end()) {
            std::cout << "Could not determine action" << std::endl;
            continue;
        } else {
            member_function_ptr_type fun_ptr = action_fun_it->second;
            event_success = ((*this).*fun_ptr)(user_word_input);
            if (!event_success) {
                continue;
            }
        }

        break;
    }
    return false;
}

std::vector<std::string> miniwumpus::Player::getUserInput() {
    //http://stackoverflow.com/questions/5607589/right-way-to-split-an-stdstring-into-a-vectorstring
    std::string user_input_args;
    std::cout << "input action \n";
    getline(std::cin, user_input_args);
    std::stringstream ss(user_input_args);
    std::istream_iterator<std::string> begin(ss);
    std::istream_iterator<std::string> end;
    std::vector<std::string> vectorize_user_input(begin, end);
    vectorize_user_input.emplace_back("filler", "filler");
    return vectorize_user_input;
}
