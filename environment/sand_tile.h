//
// Created by Erik on 7/5/2019.
//

#ifndef MINI_WUMPUS_SAND_TILE_H
#define MINI_WUMPUS_SAND_TILE_H


#include "base_tile.h"

namespace miniwumpus {
    class SandTile : public BaseTile {
    public:
        explicit SandTile(int tile_id);
    };
}

#endif //MINI_WUMPUS_SAND_TILE_H
