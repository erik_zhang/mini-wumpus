//
// Created by Erik on 7/5/2019.
//

#include "wumpus.h"

bool miniwumpus::Wumpus::take_damage(int dmg) {
    //
    std::cout << "Wumpus natural armor reduces dmg by 1" << std::endl;
    health -= (dmg - 1);
    return true;
}

bool miniwumpus::Wumpus::perform_action() {
    std::cout << "Wumpus is to lazy to perform any action and stands still" << std::endl;
    return false;
}

miniwumpus::Wumpus::Wumpus(int character_id, std::weak_ptr<EnvMap> ptr) : BaseUnit(character_id, std::move(ptr)) {

}
