CC = g++
rCC = clang++

FLAGS = -std=c++11 -g -Wall


main.exe: main.cpp obj/game_engine.o obj/base_tile.o obj/base_unit.o obj/env_map.o obj/player.o obj/sand_tile.o obj/wumpus.o
	$(CC) $(FLAGS) -o main.exe main.cpp obj/game_engine.o obj/base_tile.o obj/base_unit.o obj/env_map.o obj/player.o obj/sand_tile.o obj/wumpus.o

obj/game_engine.o : game_engine.cpp obj/base_tile.o obj/base_unit.o obj/env_map.o obj/player.o obj/sand_tile.o obj/wumpus.o
	$(CC) $(FLAGS) -c game_engine.cpp -o obj/game_engine.o

obj/base_unit.o : character/base_unit.cpp enums/directions.h
	$(CC) $(FLAGS) -c character/base_unit.cpp -o obj/base_unit.o

obj/player.o : character/player.cpp obj/base_unit.o
	$(CC) $(FLAGS) -c character/player.cpp -o obj/player.o

obj/wumpus.o : character/wumpus.cpp obj/base_unit.o
	$(CC) $(FLAGS) -c character/wumpus.cpp -o obj/wumpus.o

obj/env_map.o : environment/env_map.cpp enums/directions.h
	$(CC) $(FLAGS) -c environment/env_map.cpp -o obj/env_map.o

obj/base_tile.o : environment/base_tile.cpp
	$(CC) $(FLAGS) -c environment/base_tile.cpp -o obj/base_tile.o

obj/sand_tile.o : environment/sand_tile.cpp obj/base_tile.o
	$(CC) $(FLAGS) -c environment/sand_tile.cpp -o obj/sand_tile.o

cleanwin:
	del main.out
	del main.exe
	del /q /s obj\*

clean:
	rm -rf obj/*.o *.out && clear